'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
// const validator = require('../utils/validator');
// const queryParser = require('../utils/query_parser');
const commandHandler = require('../repositories/commands/command_handler');
const insertZip = async (req,res,next) => {
    const insertZip = async () => {
        
        return await commandHandler.insertZip(req.params);
    }
    const sendResponse = (result) => {
        (result.err) ? wrapper.response(res,'error',result) : wrapper.response(res,'success',result);
    }
    sendResponse(await insertZip());
}

module.exports = {
    insertZip: insertZip
}